﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScript : MonoBehaviour {


  public Text text;
  public PlayerMovement playerMovement;
  public GameObject gold;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if(collision.gameObject.tag == "Player")
    {
      playerMovement.speed = 0;
      text.text = "YOU WIN!";
      Instantiate(gold, playerMovement.gameObject.transform.position, Quaternion.identity);
    }
  }
}
