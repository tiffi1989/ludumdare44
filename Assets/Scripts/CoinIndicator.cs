﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinIndicator : MonoBehaviour {

  public PlayerShooting playerShooting;
  private Image image;
  private float inc = -0.05f;
	// Use this for initialization
	void Start () {
    image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    if(playerShooting.hasCoin)
    {
      image.color = Color.white;
    } else
    {
      Color currentColor = image.color;
      if(image.color.g < .01f)
      {
        inc = 0.05f;
      }
      if (image.color.g > 0.75f)
        inc = -0.05f;

     image.color = new Color(1, image.color.g + inc, 1);

    }

  }
}
