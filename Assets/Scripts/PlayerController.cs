﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

  public int currentHealth;
  public int maxHealth;
  protected bool invincible = false;
  protected SpriteRenderer rend;
  protected object playerShooting;
  protected float inc = -.1f;
  public GameObject blood;
  public InfoText infoText;
  protected  GameController gameController;
  private AudioSource audioSource;
  public AudioClip clip;

  // Use this for initialization
  void Start()
  {
    rend = GetComponent<SpriteRenderer>();
    infoText = GameObject.Find("InfoText").GetComponent<InfoText>();
    gameController = Camera.main.GetComponent<GameController>();
    audioSource = GetComponent<AudioSource>();
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if (currentHealth <= 0)
    {
      Die();
    }
    if (invincible)
      Blink();
    else
      rend.color = Color.white;
  }

  void Die()
  {
    Instantiate(blood, transform.position, Quaternion.identity);
    GameObject.Find("BigText").GetComponent<Text>().text = "YOU DIED!!";
    infoText.SetText("Press and hold R to restart!", 10000, 1.5f);
    gameController.deactivated = true;
    GameObject.Destroy(gameObject);


  }




  void Blink()
  {

    Color currentColor = rend.color;
    if (rend.color.g < .01f)
    {
      inc = 0.1f;
    }
    if (rend.color.g > 0.75f)
      inc = -0.1f;

    rend.color = new Color(1, rend.color.g + inc, 1);

  }

  void StopInvincibility()
  {
    invincible = false;
  }

  private void OnTriggerStay2D(Collider2D collision)
  {
    if ((collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Fire") && !invincible)
    {
      currentHealth--;
      audioSource.clip = clip;
      audioSource.Play();
      invincible = true;
      Invoke("StopInvincibility", 1f);
    }
  }


  private void OnCollisionStay2D(Collision2D collision)
  {
    if ((collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Fire") && !invincible)
    {
      currentHealth--;
      audioSource.clip = clip;
      audioSource.Play();
      invincible = true;
      Invoke("StopInvincibility", 1f);
    }
  }
}
