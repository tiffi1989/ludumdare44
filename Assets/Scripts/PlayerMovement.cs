﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

  public float speed;
  public float maxSpeed;

  private Rigidbody2D rig;

	void Start () {
    rig = GetComponent<Rigidbody2D>();

	}

	
	void FixedUpdate () {
    float moveHor = Input.GetAxis("Horizontal");

    float moveVer = Input.GetAxis("Vertical");

    Vector2 movement = new Vector2(moveHor, moveVer);

    if(rig.velocity.magnitude < maxSpeed)
    rig.AddForce(movement * speed);

    if (rig.velocity.x < 0)
    {
      transform.localScale = new Vector3(1, 1, 1);
    }
    else
    {
      transform.localScale = new Vector3(-1, 1, 1);
    }
  }
}
