﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveMe : MonoBehaviour {
  public float timeToLive;
	// Use this for initialization
	void Start () {
    Invoke("Die", timeToLive);
	}

  void Die()
  {
    GameObject.Destroy(gameObject);
  }
	
	// Update is called once per frame
	void Update () {
		
	}
}
