﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEatController : EnemyController {
  private Animator ani;
  public GameObject coinPrefab;
  private AudioSource audioSource;
  // Use this for initialization
  override protected void Start () {
    base.Start();
    ani = GetComponent<Animator>();
    audioSource = GetComponent<AudioSource>();
  }
	
	// Update is called once per frame
	void Update () {
	}

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if((collision.gameObject.tag =="Coin"|| collision.gameObject.tag == "SlowCoin") && !ani.GetBool("Full"))
    {
      ani.SetBool("Full", true);
      disabled = true;
      Invoke("Explode", 3);
      rig.velocity = new Vector2(0, 0);
      audioSource.Play();
      GameObject.Destroy(collision.gameObject);
    }
    Debug.Log("hallo");
  }

  void Explode()
  {
    GameObject coin = Instantiate(coinPrefab, transform.position, Quaternion.identity);
    Instantiate(bloodPrefab, transform.position, Quaternion.identity);
    coin.GetComponent<Rigidbody2D>().AddForce(100 * new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
    coin.GetComponentInChildren<Coin>().SetActive() ;
    gameController.UnregisterEnemy(gameObject);

    GameObject.Destroy(gameObject);
  }
}
