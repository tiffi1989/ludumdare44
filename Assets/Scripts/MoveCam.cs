﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCam : MonoBehaviour
{

  private bool move;
  public float moveSpeed;
  private Vector3 camDestination = new Vector3(0, 0, -10);
  private Vector3 startPosition = new Vector3(0, 0, -10);
  public enum Direction { Up, Down, Left, Right };
  private float startTime = 0;
  private float journeyLength = 0;
  private GameController gameController;

  // Use this for initialization
  void Start()
  {
    gameController = Camera.main.GetComponent<GameController>();
  }

  // Update is called once per frame
  void Update()
  {

    Vector3 camPos = Camera.main.transform.position;

    float distCovered = (Time.realtimeSinceStartup - startTime) * moveSpeed;
    if(journeyLength > 0)
    {
      float fracJourney = distCovered / journeyLength;

      Camera.main.transform.position = Vector3.Lerp(startPosition, camDestination, fracJourney);
    }

    if(camPos == camDestination && gameController.gameStarted)
    {
      Time.timeScale = 1;
      journeyLength = 0;
    }
  
  }

  public void MoveCamToNextRoom(Direction dir)
  {
    startTime = Time.realtimeSinceStartup;
    camDestination = Camera.main.transform.position;
    startPosition = Camera.main.transform.position;

    switch (dir)
    {
      case Direction.Up:
        camDestination.y += 10;

        break;
      case Direction.Down:
        camDestination.y -= 10;
        break;
      case Direction.Left:
        camDestination.x -= 16;
        break;
      case Direction.Right:
        camDestination.x += 16;
        break;
    }
    journeyLength = Vector3.Distance(startPosition, camDestination);

  }
}
