﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartContainer : MonoBehaviour {

  public PlayerController player;
  public GameObject heart;
  public Sprite fullHeartSprite;
  public Sprite emptyHeartSprite;

  private List<GameObject> heartList = new List<GameObject>();
  private int playerHealthLast = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(player.maxHealth > heartList.Count)
    {
      while(player.maxHealth > heartList.Count)
      {
        GameObject newHeart = Instantiate(heart, new Vector3(heartList.Count * 120, 0, 0), Quaternion.identity);
        newHeart.transform.SetParent(transform, false);
        heartList.Add(newHeart);
      }

    }

    if(player.currentHealth != playerHealthLast )
    {
      playerHealthLast = player.currentHealth;
      for (int i = 0; i < heartList.Count; i++)
      {
        if(player.currentHealth > i)
        {
          heartList[i].GetComponent<Image>().sprite = fullHeartSprite;
        } else
        {
          heartList[i].GetComponent<Image>().sprite = emptyHeartSprite;
        }
      }
    }
	}
}
