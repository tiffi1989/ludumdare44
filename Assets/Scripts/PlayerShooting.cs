﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

  public bool hasCoin = true;
  public GameObject coinPrefab;
  public float speed;
  private AudioSource audioSource;
  public AudioClip clip;
  private GameController gameController;

	// Use this for initialization
	void Start () {
    audioSource = GetComponent<AudioSource>();
    gameController = Camera.main.GetComponent<GameController>();

  }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButton("Fire1") && hasCoin && gameController.gameStarted)
    {
      Shoot();
    }
	}

  void Shoot()
  {
    hasCoin = false;
    GameObject coin = Instantiate(coinPrefab, new Vector3(transform.position.x, transform.position.y , 0), Quaternion.identity);
    Rigidbody2D coinRig = coin.GetComponent<Rigidbody2D>();
    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    Vector3 mouseDir = mousePos - gameObject.transform.position;
    mouseDir.z = 0.0f;
    mouseDir = mouseDir.normalized;
    coinRig.AddForce(mouseDir * speed);
    coinRig.AddTorque(Random.Range(0,4));
  }

  public void GiveCoin()
  {
    hasCoin = true;
    audioSource.clip = clip;
    audioSource.Play();
  }
}
