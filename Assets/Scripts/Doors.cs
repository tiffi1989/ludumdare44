﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Doors : MonoBehaviour
{

  private GameController gameController;
  private TilemapRenderer tilemapRenderer;
  private TilemapCollider2D tilemapCollider;
  private AudioSource audioSource;
  private float timer;
  private PlayerController playerController;
  private int roomCounter;
  // Use this for initialization
  void Start()
  {
    gameController = Camera.main.GetComponent<GameController>();
    tilemapRenderer = GetComponent<TilemapRenderer>();
    tilemapCollider = GetComponent<TilemapCollider2D>();
    audioSource = GetComponent<AudioSource>();
    playerController = GameObject.Find("Player").GetComponent<PlayerController>();

  }

  // Update is called once per frame
  void FixedUpdate()
  {
    timer += Time.deltaTime;
    if (gameController.doorsOpen && tilemapRenderer.enabled)
    {
      tilemapRenderer.enabled = false;
      tilemapCollider.enabled = false;
      roomCounter++;
      if (roomCounter > 2)
        playerController.currentHealth = 3;
      if (timer > 3)
        audioSource.Play();
    }
    if (!gameController.doorsOpen && !tilemapRenderer.enabled)
    {
      tilemapRenderer.enabled = true;
      tilemapCollider.enabled = true;
    }
  }
}
