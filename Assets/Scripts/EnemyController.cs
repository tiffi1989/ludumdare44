﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

  protected GameObject player;
  public int health;
  protected bool dead = false;
  protected Rigidbody2D rig;
  public float speed;
  protected bool invincible;
  public GameObject bloodPrefab;
  protected SpriteRenderer rend;
  protected GameController gameController;
  protected bool disabled;

  // Use this for initialization
  protected virtual void Start()
  {
    player = GameObject.Find("Player");
    rig = GetComponent<Rigidbody2D>();
    rend = GetComponent<SpriteRenderer>();
    gameController = Camera.main.GetComponent<GameController>();
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if(!rend.isVisible || disabled)
    {
      return;
    } 

    if (health <= 0 && !dead)
    {
      // Invoke?
      Die();
    }
    if (!dead)
    {
      Move();
    }
  }

  void Move()
  {
    if (rig.velocity.x < 0)
    {
      transform.localScale = new Vector3(1, 1, 1);
    }
    else
    {
      transform.localScale = new Vector3(-1, 1, 1);
    }
    Vector3 playerPos = player.transform.position;
    Vector3 moveDir = playerPos - gameObject.transform.position;
    moveDir.z = 0.0f;
    moveDir = moveDir.normalized;
    rig.AddForce(moveDir * speed);


  }

  void Die()
  {
    gameController.UnregisterEnemy(gameObject);
    GameObject.Destroy(gameObject);
  }

  void StopInvincibility()
  {
    invincible = false;
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if(collision.gameObject.tag == "MainCamera")
    {
      gameController.RegisterEnemy(gameObject);

    }
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.tag == "Coin" && !invincible)
    {
      health--;

      Vector3 coinPos = collision.gameObject.transform.position;
      Vector3 enemyPos = transform.position;
      Vector3 direction = enemyPos - coinPos;
      direction.z = 0;
      Instantiate(bloodPrefab, transform.position, Quaternion.LookRotation(direction));
      invincible = true;
      Invoke("StopInvincibility", .5f);
    }
  }


}
