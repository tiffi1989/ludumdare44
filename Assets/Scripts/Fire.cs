﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

  public float duration0;
  public float duration1;
  public float duration2;
  public float delay;
  private int currentStep;
  private Animator ani;
  private CircleCollider2D col;
  private Light lig;
  private bool active;
  private SpriteRenderer rend;
  public bool igniteForever;
  public float intensityModify;
  

  // Use this for initialization
  void Start()
  {
    ani = GetComponent<Animator>();
    col = GetComponent<CircleCollider2D>();
    lig = GetComponentInChildren<Light>();
    rend = GetComponent<SpriteRenderer>();
    lig.intensity += intensityModify; 
  }

  void Next()
  {
    Debug.Log("next");
    ani.SetTrigger("Next");
    switch (currentStep)
    {
      case 0:
        currentStep++;
        Invoke("Next", duration0);
        col.enabled = false;
        lig.enabled = false;
        break;
      case 1:
        currentStep++;
        Invoke("Next", duration1);
        lig.enabled = true; break;

      case 2:
        currentStep = 0;
        col.enabled = true;
        Invoke("Next", duration2); break;
    }
  }

  // Update is called once per frame
  void Update()
  {
    if (rend.isVisible && !active)
    {
      active = true;
      currentStep = 1;
      
      Invoke("Next", duration0 +delay);
    }
    if (!rend.isVisible && active && !igniteForever)
    {
      active = false;
      CancelInvoke();
    }

  }
}
