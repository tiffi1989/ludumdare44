﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

  private bool isActive = false;
  

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

  private void OnTriggerExit2D(Collider2D collision)
  {
    if(collision.gameObject.tag == "Player" )
    {
      isActive = true;
    }
  }

  public void SetActive()
  {
    gameObject.layer = 13;
    isActive = true;
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if (collision.gameObject.tag == "Player" && isActive)
    {
      PlayerShooting ps = collision.gameObject.GetComponent<PlayerShooting>();
      ps.GiveCoin();
      GameObject.Destroy(transform.parent.gameObject);
    }
  }


}
