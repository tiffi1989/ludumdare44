﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {

  private Animator ani;
  public float minTime;
  public float maxTime;
  public float duration;
  private float time, timer = 0;
  private bool isActive;
  public float strength;
  private SpriteRenderer spriteRenderer;

  

	// Use this for initialization
	void Start () {
    ani = GetComponent<Animator>();
    time = Random.Range(minTime, maxTime);
    spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    if (!spriteRenderer.isVisible)
      return;
    timer += Time.deltaTime;

    if(timer >= time && !isActive)
    {
      Activate();
      Invoke("Deactivate", duration);
    }
    if(isActive)
    {
      GameObject coin = GameObject.FindGameObjectWithTag("Coin");
      if(!coin)
      {
        coin = GameObject.FindGameObjectWithTag("SlowCoin");
      }


      if (coin)
      {
        Vector3 coinPos = coin.transform.position;
        Vector3 magnetPos = transform.position;
        Vector3 direction = magnetPos - coinPos;
        direction.z = 0;
        coin.GetComponent<Rigidbody2D>().AddForce(direction * strength);
      }

    }
	}

  void Activate()
  {
    ani.SetBool("Active", true);
    isActive = true;
  }

  void Deactivate()
  {
    ani.SetBool("Active", false);
    isActive = false;
    timer = 0;
    time = Random.Range(minTime, maxTime);
  }

  
}
