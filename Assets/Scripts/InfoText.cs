﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoText : MonoBehaviour {

  private float timer;
  private float delay;
  private float duration;
  private string text;
  private bool delayed;
  private Text textComponent;

	// Use this for initialization
	void Start () {
    textComponent = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    timer += Time.deltaTime;

    if (timer >= delay)
      delayed = false;

    if(!delayed)
    {
      textComponent.text = text;
    }

    if(timer >= duration+delay)
    {
      textComponent.text = "";
    }

	}

  public void SetText(string text, float duration, float delay) {
    timer = 0;
    this.delay = delay;
    this.duration = duration;
    this.text = text;
    delayed = true;
  }

  
}
