﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTooFast : MonoBehaviour {

  public float minimumCoinSpeed;

  private Rigidbody2D rig;
	// Use this for initialization
	void Start () {
    rig = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
    if (rig.velocity.magnitude < minimumCoinSpeed)
      gameObject.tag = "SlowCoin";
    else
      gameObject.tag = "Coin";
	}
}
