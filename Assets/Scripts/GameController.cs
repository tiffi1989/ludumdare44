﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
  private float timer;
  public bool doorsOpen;
  private HashSet<GameObject> enemiesInRoom = new HashSet<GameObject>();
  public InfoText infoText;
  private PlayerShooting playerShooting;
  public MoveCam moveCam;
  private GameObject player;
  public bool deactivated;
  public GameObject tutorial;
  public bool gameStarted;
  // Use this for initialization
  void Start()
  {
    player = GameObject.Find("Player");
    playerShooting = player.GetComponent<PlayerShooting>();
    Time.timeScale = 0;
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    if (Input.GetKey(KeyCode.R))
    {
      timer += Time.deltaTime;
      if (timer > 1f)
        SceneManager.LoadScene("MainScene");
    }
    else
    {
      timer = 0;
    }

    CheckEnemies();
  }

  void CheckEnemies()
  {
    if (enemiesInRoom.Count > 0)
      doorsOpen = false;
    else
    {
      if (!playerShooting.hasCoin)
      {
        infoText.SetText("Get your coin!!!!", .1f, 0);
        doorsOpen = false;

      }
      else
        doorsOpen = true;

    }
  }

  public void RegisterEnemy(GameObject enemy)
  {
    enemiesInRoom.Add(enemy);

  }

  public void UnregisterEnemy(GameObject enemy)
  {
    enemiesInRoom.Remove(enemy);

  }

  private void OnTriggerExit2D(Collider2D collision)
  {
    if (collision.gameObject.tag == "Player" && Time.timeScale != 0 && !deactivated)
    {
      Time.timeScale = 0;
      player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
      Vector3 dir = player.transform.position - transform.position;
      if (dir.y > 6)
        moveCam.MoveCamToNextRoom(MoveCam.Direction.Up);
      else if (dir.y < -6)
        moveCam.MoveCamToNextRoom(MoveCam.Direction.Down);
      else if (dir.x > 9)
        moveCam.MoveCamToNextRoom(MoveCam.Direction.Right);
      else if (dir.x < -9)
        moveCam.MoveCamToNextRoom(MoveCam.Direction.Left);


    }
  }

  public void StartGame()
  {
    Time.timeScale = 1;
    tutorial.SetActive(false);
    gameStarted = true;
  }
}
