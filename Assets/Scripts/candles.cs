﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class candles : MonoBehaviour {

  private Light pointLight;
  private float inc;
  private float incStep;
  private SpriteRenderer spriteRenderer;

  // Use this for initialization
  void Start () {
    pointLight = GetComponentInChildren<Light>();
    incStep = Random.Range(0.01f, 0.02f);
    inc = -incStep;
    spriteRenderer = GetComponent<SpriteRenderer>();

  }

  // Update is called once per frame
  void FixedUpdate () {
    if(!spriteRenderer.isVisible)
    {
      pointLight.enabled = false;
      return;
    } else
    {
      pointLight.enabled = true;
    }
    if (pointLight.color.b < .3f)
    {
      inc = incStep;
    }
    if (pointLight.color.b > 0.5f)
      inc = -incStep;

    pointLight.color = new Color(pointLight.color.r, pointLight.color.g, pointLight.color.b + inc);
  }
}
